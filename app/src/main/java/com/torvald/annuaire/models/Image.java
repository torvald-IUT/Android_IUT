package com.torvald.annuaire.models;

import java.io.Serializable;

/**
 * Created by Quentin LAURENT on 15/02/2018.
 */

public class Image implements Serializable {
    private int img;

    public Image(int img) {
        this.img = img;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
