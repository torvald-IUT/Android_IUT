package com.torvald.annuaire.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by torvald on 20/03/18.
 */

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public static final int databaseVersion = 2;
    public static final String databaseName = "contact.db";

    private static final String SQLCreateTablePerson =
            "CREATE TABLE IF NOT EXISTS " + ContactDatabase.Person.tableName + " (" +
                    ContactDatabase.Person._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ContactDatabase.Person.columnLastName + " " +
                    ContactDatabase.Person.columnLastNameType + "," +
                    ContactDatabase.Person.columnFirstName + " " +
                    ContactDatabase.Person.columnFirstNameType + "," +
                    ContactDatabase.Person.columnAge + " " +
                    ContactDatabase.Person.columnAgeType + "," +
                    ContactDatabase.Person.columnImg + " " +
                    ContactDatabase.Person.columnImgType + ")";

    private static final String SQLDeleteTableContact =
            "DROP TABLE IF EXIST " + ContactDatabase.Person.tableName;

    public DatabaseOpenHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQLCreateTablePerson);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQLDeleteTableContact);
        onCreate(sqLiteDatabase);
    }
}
