package com.torvald.annuaire;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    private static String LOGIN = "user@mail.com";
    private static String PASSWORD = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Création de l'activité
        super.onCreate(savedInstanceState);
        // On cache la barre d'action
        getSupportActionBar().hide();
        // On affiche le contenu de l'activité
        setContentView(R.layout.activity_login);

        // Définition des champs de texte
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        // Définition du bouton de connexion
        final Button button = findViewById(R.id.email_sign_in_button);

        // Récupération du contexte
        final Context context = getApplicationContext();

        // Définition de la durée du toast
        final int lenght = Toast.LENGTH_LONG;

        // Actions à effectuer au clic sur le bouton
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!mEmailView.getText().toString().equals(LOGIN)) {
                    // Appel au texte d'erreur
                    String errorMessage = getString(R.string.login_error);

                    // Création du toast
                    Toast.makeText(context, errorMessage, lenght).show();
                } else if (!mPasswordView.getText().toString().equals(PASSWORD)){
                    // Appel au texte d'erreur
                    String errorMessage = getString(R.string.password_error);

                    // Création du toast
                    Toast.makeText(context, errorMessage, lenght).show();
                } else {
                    // Appel au texte de login
                    String connectedText = getString(R.string.welcome_message, mEmailView.getText().toString());

                    // Création du toast
                    Toast.makeText(context, connectedText, lenght).show();

                    // Changement d'activité
                    Intent intent = new Intent(LoginActivity.this, ListeContactActivity.class);
                    intent.putExtra("email", mEmailView.getText());
                    LoginActivity.this.startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        // Définition des champs de texte
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        if (mEmailView.getText() != null) {
            mEmailView.setText("");
        }

        if (mPasswordView.getText() != null) {
            mPasswordView.setText("");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}

