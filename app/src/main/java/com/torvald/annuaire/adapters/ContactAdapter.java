package com.torvald.annuaire.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.torvald.annuaire.models.Contact;
import com.torvald.annuaire.R;
import com.torvald.annuaire.models.Image;

import java.util.List;

/**
 * Created by ql269 on 05/02/2018.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyContactHolder> {
    private List<Contact> contactList;

    class MyContactHolder extends RecyclerView.ViewHolder {
        TextView first_name;
        TextView last_name;
        TextView age;
        ImageView img;

        MyContactHolder(View view) {
            super(view);
            first_name = (TextView) view.findViewById(R.id.contact_first_name);
            last_name = (TextView) view.findViewById(R.id.contact_last_name);
            age = (TextView) view.findViewById(R.id.contact_age);
            img = (ImageView) view.findViewById(R.id.contact_img);
        }
    }

    public ContactAdapter(List<Contact> contactList) {
        this.contactList = contactList;
    }

    @Override
    public MyContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_row, parent, false);

        return new MyContactHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyContactHolder holder, int position) {
        Contact contact = contactList.get(position);
        holder.first_name.setText(contact.getFirstName());
        holder.last_name.setText(contact.getLastName());
        holder.age.setText(Integer.toString(contact.getAge()));
        holder.img.setImageResource(contact.getImg());
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
}
