package com.torvald.annuaire;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.torvald.annuaire.adapters.ImgAddContactAdapter;
import com.torvald.annuaire.models.Image;

import java.util.ArrayList;
import java.util.List;

public class AddContactActivity extends AppCompatActivity {
    private List<Image> imageList = new ArrayList<>();
    private RecyclerView recyclerView;

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mAge;
    private int mImage;
//    DatabaseOpenHelper databaseOpenHelper = new DatabaseOpenHelper(this);
//    DBContact dbContact = new DBContact(databaseOpenHelper);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        recyclerView =  (RecyclerView) findViewById(R.id.imgRecylerView);
        recyclerView.setAdapter(new ImgAddContactAdapter(imageList, R.layout.img_recycler_view_add));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    int selected;
                    View beforeView;
                    @Override
                    public void onItemClick(View view, int position) {
                        if (!(selected == position)) {
                            selected = position;
                        }
                        mImage = imageList.get(selected).getImg();
                        view.setBackgroundColor(Color.RED);
                        Toast.makeText(getApplicationContext(), "Item Clicked " + mImage, Toast.LENGTH_LONG).show();

                        if (beforeView != null) {
                            beforeView.setBackgroundColor(Color.TRANSPARENT);
                        }
                        beforeView = view;
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                })
        );

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        prepareImageData();

        mFirstName = findViewById(R.id.AddFirstName);
        mLastName = findViewById(R.id.AddLastName);
        mAge = findViewById(R.id.AddAge);
        final Button mSaveButton = (Button) findViewById(R.id.add_contact_button);

        mFirstName.addTextChangedListener(new TextValidator(mFirstName) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.length() < 3 || textView.length() > 15) {
                    text = getString(R.string.first_name_invalid_characters_number, "3", "15");
                    textView.setError(text);
                }
            }
        });

        mFirstName.addTextChangedListener(new TextValidator(mFirstName) {
            @Override
            public void validate(TextView textView, String text) {
                if (textView.length() < 3 || textView.length() > 15) {
                    text = getString(R.string.last_name_invalid_characters_number, "3", "25");
                    textView.setError(text);
                }
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mFirstName.getText().toString().isEmpty() && !mLastName.getText().toString().isEmpty()) {
//                    Contact contact = new Contact(mLastName.getText().toString(), mFirstName.getText().toString(), Integer.parseInt(mAge.getText().toString()), 1, mImage);
//                    dbContact.insert(contact);
                    Intent intent = new Intent(AddContactActivity.this, ListeContactActivity.class);
                    intent.putExtra("lastName", mLastName.getText().toString());
                    intent.putExtra("firstName", mFirstName.getText().toString());
                    intent.putExtra("age", Integer.parseInt(mAge.getText().toString()));
                    intent.putExtra("image", mImage);

                    setResult(AddContactActivity.RESULT_OK, intent);
                    finish();

                } else if (mFirstName.getText().toString().isEmpty() && mLastName.getText().toString().isEmpty()){
                    mFirstName.setError(getString(R.string.field_not_empty));
                    mLastName.setError(getString(R.string.field_not_empty));
                } else if (mFirstName.getText().toString().isEmpty()){
                    mFirstName.setError(getString(R.string.field_not_empty));
                } else if (mLastName.getText().toString().isEmpty()) {
                    mLastName.setError(getString(R.string.field_not_empty));
                }
            }
        });
    }

    private void prepareImageData() {
        Image image = new Image(R.drawable.charmander);
        imageList.add(image);

        image = new Image(R.drawable.bulbasaur);
        imageList.add(image);

        image = new Image(R.drawable.squirtle);
        imageList.add(image);

        image = new Image(R.drawable.pikachu);
        imageList.add(image);

//        ImgAddContactAdapter.notifyDataSetChanged();
    }
}
